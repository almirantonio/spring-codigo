package com.example.demo.Model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Aves extends Animal {
	
	private int peso;
	private String vacina;
	private String  data_vacinacao ;
	private String  data_nascimento ;
	
	public Aves() {}
	
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
	}
	public String getVacina() {
		return vacina;
	}
	public void setVacina(String vacina) {
		this.vacina = vacina;
	}

	public String getData_vacinacao() {
		return data_vacinacao;
	}

	public void setData_vacinacao(String data_vacinacao) {
		this.data_vacinacao = data_vacinacao;
	}

	public String getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	
	
}
