package com.example.demo.Model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Animal_Leiteiro  extends Animal{
	private int quantidade_leite;
	private String data_cio;
	private String data_paricao;
	
	public Animal_Leiteiro () {}
	
	public int getQuantidade_leite() {
		return quantidade_leite;
	}
	public void setQuantidade_leite(int quantidade_leite) {
		this.quantidade_leite = quantidade_leite;
	}

	public String getData_cio() {
		return data_cio;
	}

	public void setData_cio(String data_cio) {
		this.data_cio = data_cio;
	}

	public String getData_paricao() {
		return data_paricao;
	}

	public void setData_paricao(String data_paricao) {
		this.data_paricao = data_paricao;
	}
	
	
	

	
}
