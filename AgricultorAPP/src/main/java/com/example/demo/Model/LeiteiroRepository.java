package com.example.demo.Model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LeiteiroRepository extends JpaRepository<Animal_Leiteiro,Integer> {

}
