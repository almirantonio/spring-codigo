package com.example.demo.Model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RacaoRepository extends JpaRepository<Racao,Integer> {

}
