package com.example.demo.Model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CorteRepository extends JpaRepository<Animal_Corte,Integer> {

}
