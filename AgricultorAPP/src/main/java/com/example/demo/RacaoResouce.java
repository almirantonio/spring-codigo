package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.example.demo.Model.Racao;
import com.example.demo.Model.RacaoRepository;

@RestController
@RequestMapping("/racao")
@CrossOrigin
public class RacaoResouce {
	
	@Autowired
	private RacaoRepository rr;

	@GetMapping
	public List<Racao> listar() {
		return rr.findAll();
	}

	@GetMapping("/{id}")
	public Racao listarFuncionario(@PathVariable("id") int id) {
		return rr.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Racao> novoAluno(@RequestBody Racao a) {
		Racao pRacao = rr.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pRacao.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pRacao);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		rr.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Racao> atualizar(@PathVariable int id, @RequestBody Racao r){
		Optional<Racao> pRacao=rr.findById(id);
		BeanUtils.copyProperties(r, pRacao.get(),"id");
		rr.save(pRacao.get());
		return ResponseEntity.ok(pRacao.get());
		
	}


}
