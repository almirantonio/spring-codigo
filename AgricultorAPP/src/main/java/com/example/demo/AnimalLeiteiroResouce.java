package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Model.Animal_Leiteiro;
import com.example.demo.Model.LeiteiroRepository;

@RestController
@RequestMapping("/animal/leiteiro")
@CrossOrigin
public class AnimalLeiteiroResouce {

	@Autowired
	private LeiteiroRepository al;

	@GetMapping
	public List<Animal_Leiteiro> listar() {
		return al.findAll();
	}

	@GetMapping("/{id}")
	public Animal_Leiteiro listar(@PathVariable("id") int id) {
		return al.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Animal_Leiteiro> novoAluno(@RequestBody Animal_Leiteiro a) {
		Animal_Leiteiro pl = al.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pl.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pl);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		al.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Animal_Leiteiro> atualizar(@PathVariable int id, @RequestBody Animal_Leiteiro a){
		Optional<Animal_Leiteiro> pl=al.findById(id);
		BeanUtils.copyProperties(a, pl.get(),"id");
		al.save(pl.get());
		return ResponseEntity.ok(pl.get());
		
	}

	
}
