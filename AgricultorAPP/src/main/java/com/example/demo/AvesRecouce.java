package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.example.demo.Model.Aves;
import com.example.demo.Model.AvesRepository;

@RestController
@RequestMapping("/animal/aves")
@CrossOrigin
public class AvesRecouce {

	@Autowired
	private AvesRepository av;

	@GetMapping
	public List<Aves> listar() {
		return av.findAll();
	}

	@GetMapping("/{id}")
	public  Aves listarFuncionario(@PathVariable("id") int id) {
		return av.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Aves> novoAluno(@RequestBody Aves a) {
		 Aves pAves = av.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pAves.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pAves);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		av.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Aves> atualizar(@PathVariable int id, @RequestBody Aves a){
		Optional<Aves> pAves=av.findById(id);
		BeanUtils.copyProperties(a, pAves.get(),"id");
		av.save(pAves.get());
		return ResponseEntity.ok(pAves.get());
		
	}
}
