package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Model.Animal;
import com.example.demo.Model.AnimalRepository;



@RestController
@RequestMapping("/animal")
@CrossOrigin
public class AnimalResouce {
	@Autowired
	private AnimalRepository ar;

	@GetMapping
	public List<Animal> listar() {
		return ar.findAll();
	}

	@GetMapping("/{id}")
	public  Animal listarFuncionario(@PathVariable("id") int id) {
		return ar.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Animal> novoAluno(@RequestBody Animal a) {
		 Animal pAnimal = ar.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pAnimal.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pAnimal);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		ar.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Animal> atualizar(@PathVariable int id, @RequestBody Animal a){
		Optional<Animal> pAnimal=ar.findById(id);
		BeanUtils.copyProperties(a, pAnimal.get(),"id");
		ar.save(pAnimal.get());
		return ResponseEntity.ok(pAnimal.get());
		
	}
}
