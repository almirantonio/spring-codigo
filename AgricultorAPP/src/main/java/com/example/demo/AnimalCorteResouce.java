package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Model.Animal_Corte;
import com.example.demo.Model.CorteRepository;

@RestController
@RequestMapping("/animal/corte")
@CrossOrigin
public class AnimalCorteResouce {
	@Autowired
	private CorteRepository ac;

	@GetMapping
	public List<Animal_Corte> listar() {
		return ac.findAll();
	}

	@GetMapping("/{id}")
	public Animal_Corte listar(@PathVariable("id") int id) {
		return ac.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Animal_Corte> novoAluno(@RequestBody Animal_Corte a) {
		Animal_Corte pc = ac.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pc.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pc);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		ac.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Animal_Corte> atualizar(@PathVariable int id, @RequestBody Animal_Corte a){
		Optional<Animal_Corte> pAves=ac.findById(id);
		BeanUtils.copyProperties(a, pAves.get(),"id");
		ac.save(pAves.get());
		return ResponseEntity.ok(pAves.get());
		
	}

}
