package com.example.demo;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Model.Funcionario;
import com.example.demo.Model.FuncionarioRepository;


@RestController
@RequestMapping("/funcionario")
@CrossOrigin
public class FuncionarioResouce {

	@Autowired
	private FuncionarioRepository fr;

	@GetMapping
	public List<Funcionario> listar() {
		return fr.findAll();
	}

	@GetMapping("/{id}")
	public Funcionario listarFuncionario(@PathVariable("id") int id) {
		return fr.findById(id).get();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Funcionario> novoAluno(@RequestBody Funcionario a) {
		Funcionario pFuncionario = fr.save(a);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/id").buildAndExpand(pFuncionario.getId())
				.toUri();
		return ResponseEntity.created(uri).body(pFuncionario);

	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable int id) {
		fr.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Funcionario> atualizar(@PathVariable int id, @RequestBody Funcionario f){
		Optional<Funcionario> pFuncionario=fr.findById(id);
		BeanUtils.copyProperties(f, pFuncionario.get(),"id");
		fr.save(pFuncionario.get());
		return ResponseEntity.ok(pFuncionario.get());
		
	}

}
